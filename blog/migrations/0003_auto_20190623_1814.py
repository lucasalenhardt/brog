# Generated by Django 2.2.2 on 2019-06-23 21:14

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0002_auto_20190618_1301'),
    ]

    operations = [
        migrations.RenameField(
            model_name='category',
            old_name='nome',
            new_name='name',
        ),
    ]
